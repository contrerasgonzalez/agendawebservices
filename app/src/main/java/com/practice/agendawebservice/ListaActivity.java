package com.practice.agendawebservice;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.practice.agendawebservice.Objetos.Contactos;
import com.practice.agendawebservice.Objetos.Device;
import com.practice.agendawebservice.Objetos.ProcesosPHP;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ListaActivity extends ListActivity implements Response.Listener<JSONObject>, Response.ErrorListener {
    private Button btnNuevo;
    final Context context = this;
    private ProcesosPHP php = new ProcesosPHP();
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private ArrayList<Contactos> contactos;
    protected String serveip = "https://formdatapp.000webhostapp.com/WebServices/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        contactos = new ArrayList<>();
        request = Volley.newRequestQueue(context);
        consultarTodosWebService();
        btnNuevo = (Button) findViewById(R.id.btnNuevo);
        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
    }

    public void consultarTodosWebService(){
        String url = serveip + "wsConsultarTodos.php?idMovil=" + Device.getSecureId(this);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url,null,this,this);
        Log.i("URL-SELECT",url);
        request.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(JSONObject response) {
        Contactos contacto = null;
        try {
            JSONArray jsonArray = response.getJSONArray("contactos");
            for (int i = 0;i<jsonArray.length();i++){
                contacto = new Contactos();
                JSONObject jsonObject = null;
                jsonObject = jsonArray.getJSONObject(i);
                contacto.set_ID(jsonObject.optInt("_ID"));
                contacto.setNombre(jsonObject.optString("nombre"));
                contacto.setTelefono1(jsonObject.optString("telefono1"));
                contacto.setTelefono2(jsonObject.optString("telefono2"));
                contacto.setDomicilio(jsonObject.optString("direccion"));
                contacto.setFavorito(jsonObject.optInt("favorite"));
                contacto.setNotas(jsonObject.optString("notas"));
                contacto.setIdMovil(jsonObject.optString("idMovil"));
                contactos.add(contacto);
            }
            MyArrayAdapter adapter = new MyArrayAdapter(context,R.layout.layout_contacto,contactos);
            setListAdapter(adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    class MyArrayAdapter extends ArrayAdapter<Contactos> {
        Context context;
        int textViewResourceId;
        ArrayList<Contactos> objects;

        public MyArrayAdapter(Context context, int textViewResourceId, ArrayList<Contactos> objects){
            super(context,textViewResourceId,objects);
            this.context = context;
            this.textViewResourceId = textViewResourceId;
            this.objects = objects;
        }

        public View getView(final int position, View converView, ViewGroup parent){
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(this.textViewResourceId, parent,false);
            TextView lblNombre = (TextView) view.findViewById(R.id.lblNombreContacto);
            TextView lblTelefono = (TextView) view.findViewById(R.id.lblTelefonoContacto);
            Button btnModificar = (Button) view.findViewById(R.id.btnModificar);
            Button btnBorrar = (Button) view.findViewById(R.id.btnBorrar);
            if(objects.get(position).getFavorito()>0){
                lblNombre.setTextColor(Color.BLUE);
                lblTelefono.setTextColor(Color.BLUE);
            }else{
                lblNombre.setTextColor(Color.BLACK);
                lblTelefono.setTextColor(Color.BLACK);
            }
            lblNombre.setText(objects.get(position).getNombre());
            lblTelefono.setText(objects.get(position).getTelefono1());

            btnBorrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    php.setContext(context);
                    Log.i("id",String.valueOf(objects.get(position).get_ID()));
                    php.borrarContactoWebService(objects.get(position).get_ID());
                    objects.remove(position);
                    notifyDataSetChanged();
                    Toast.makeText(getApplicationContext(),"Contacto eliminado con exito",Toast.LENGTH_SHORT).show();
                }
            });

            btnModificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("contacto",objects.get(position));
                    Intent intent = new Intent();
                    intent.putExtras(bundle);
                    setResult(Activity.RESULT_OK,intent);
                    finish();
                }
            });
            return view;
        }
    }
}
