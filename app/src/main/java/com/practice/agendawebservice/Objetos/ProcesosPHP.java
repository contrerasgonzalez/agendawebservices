package com.practice.agendawebservice.Objetos;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.ArrayList;

public class ProcesosPHP implements Response.Listener<JSONObject>, Response.ErrorListener {
    private RequestQueue request;
    private JsonObjectRequest jsonObjectRequest;
    private ArrayList<Contactos> contactos = new ArrayList<>();
    private String serverip = "https://formdatapp.000webhostapp.com/WebServices/";

    public void setContext(Context context){
        request = Volley.newRequestQueue(context);
    }

    public void insertarContactoWebService(Contactos c){
        String url = serverip + "wsRegistro.php?nombre=" + c.getNombre()
                + "&telefono1=" + c.getTelefono1()
                + "&telefono2=" + c.getTelefono2()
                + "&direccion=" + c.getDomicilio()
                + "&notas=" + c.getNotas()
                + "&favorite=" + c.getFavorito()
                + "&idMovil=" + c.getIdMovil();
        //url = url.replace("","%20");
        Log.i("URL-INSERT",url);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url,null, this,this);
        request.add(jsonObjectRequest);
    }

    public void actualizarContactoWebService(Contactos c, int id){
        String url = serverip + "wsActualizar.php?_ID=" + id
                + "&nombre=" + c.getNombre()
                + "&direccion=" + c.getDomicilio()
                + "&telefono1=" + c.getTelefono1()
                + "&telefono2=" + c.getTelefono2()
                + "&notas=" + c.getNotas()
                + "&favorite=" + c.getFavorito();
        //url = url.replace("","%20");
        Log.i("URL-UPDATE",url);
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this,this);
        request.add(jsonObjectRequest);
    }

    public void borrarContactoWebService(int id){
        String url = serverip + "wsEliminar.php?_ID=" + id;
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, this,this);
        request.add(jsonObjectRequest);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.i("ERROR",error.toString());
    }

    @Override
    public void onResponse(JSONObject response) {
        System.out.println(response.toString());
        Log.i("Response",response.toString());
    }
}
